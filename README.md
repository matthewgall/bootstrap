# README #

matthewgall/bootstrap is a simple repository that tracks the scripts that power the matthewgall-bootstrap debian package on [my repository](https://repo.matthewgall.com). The package is compiled using [fpm](https://github.com/jordansissel/fpm)

### How do I get set up? ###

Getting started with this is easy!

* First, clone the repository  
    `git clone git@bitbucket.org:matthewgall/bootstrap.git`
* Next, we need to install some dependencies  
    `gem install fpm`
* Finally, using compile.sh, compile the .deb using the following command  
    `./compile.sh`
* And you're done!  

### Who do I talk to? ###

* Open an issue using the issue tracker to the right
* Or, contact the maintainer directly by [clicking here](mailto:repo@matthewgall.com)
