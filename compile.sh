#!/bin/bash

echo "Compiling..."
fpm -f -s empty -t deb -n matthewgall-bootstrap -v 1.0.0 --post-install post-install.sh --post-uninstall post-uninstall.sh --maintainer "Matthew Gall <repo@matthewgall.com>" --vendor "Matthew Gall <repo@matthewgall.com>" --url "https://matthewgall.com" --description "A simple bootstrapping script for servers maintained by Matthew Gall" --license MIT > /dev/null 2>&1
