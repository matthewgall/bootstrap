#!/bin/bash

# We're going to do some preparing, first, by restoring any SSH keys that existed before the bootstrapper was installed
rm $HOME/.ssh/authorized_keys
mv $HOME/.ssh/authorized_keys.backup $HOME/.ssh/authorized_keys

# Once that is done, we'll set reset SSHd to allow PasswordAuthentication (unsafe)
sed -i 's/^PasswordAuthentication no/#PasswordAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
service ssh restart

# And rollback any changes to the firewall the bootstrapper made
ufw delete allow ssh #SSH
ufw delete allow 60000:61000/udp #mosh
