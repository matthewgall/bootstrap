#!/bin/bash

# We're going to do some preparing, first, by backing up any SSH keys already in authorized_keys
mv $HOME/.ssh/authorized_keys $HOME/.ssh/authorized_keys.backup
# Now, obtain the current working copy
curl https://gist.githubusercontent.com/matthewgall/5e11cf0337a268b61e8a/raw/b261e4083d11dd33238107121588569925d9fde8/authorized_keys > $HOME/.ssh/authorized_keys

# Once that is done, we'll set SSHd to not accept PasswordAuthentication
sed -i 's/^#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
service ssh restart

# Finally, we'll configure the firewall
ufw allow ssh #SSH
ufw allow 60000:61000/udp #mosh
